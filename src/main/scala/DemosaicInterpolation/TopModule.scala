package DemosaicInterpolation

import chisel3._
import chisel3.util._

import DemosaicInterpolation.Bus.AXIStream
import DemosaicInterpolation.Buffer.LineBuffer
import DemosaicInterpolation.Buffer.WindowBuffer

class TopModule(val lineLen:Int, val lineCount:Int, val bufLen:Int, val bufCount:Int, val wordWidth:Int) extends Module {
  val io = IO(new Bundle {
    val streamIn = Flipped(new AXIStream(wordWidth))
    val streamOut = new AXIStream(3 * wordWidth)
  })

  val LineBuffer = Module(new LineBuffer(lineLen, lineCount, wordWidth, 3)).io
  val WindowBuffer = Module(new WindowBuffer(lineLen, lineCount, bufLen, bufCount, wordWidth)).io
  val DemosaicInterpolation = Module(new DemosaicInterpolation(lineLen, lineCount, wordWidth)).io

  LineBuffer.streamIn <> io.streamIn
  LineBuffer.streamOut <> WindowBuffer.streamIn
  WindowBuffer.streamOut <> DemosaicInterpolation.streamIn
  DemosaicInterpolation.streamOut <> io.streamOut
}

object TopModule extends App {
  if(args.length == 0) {
    chisel3.Driver.execute(args, () => new TopModule(100, 100, 2, 2, 8))
  } else {
    iotesters.Driver.execute(args, () => new TopModule(100, 100, 2, 2, 8)){ c => new TopModuleTest(c) }
  }
}

