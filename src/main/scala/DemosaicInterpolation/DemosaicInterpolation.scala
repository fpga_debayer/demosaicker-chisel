package DemosaicInterpolation

import chisel3._
import chisel3.util._

import DemosaicInterpolation.Bus.AXIStream

class DemosaicInterpolation(val lineLen:Int, val lineCount:Int, val wordWidth:Int) extends Module {
  val io = IO(new Bundle{
    val streamIn = Flipped(new AXIStream(wordWidth * 4))
    val streamOut = new AXIStream(wordWidth * 3)
  })

  val pxlCnt = RegInit(0.U(12.W))
  val lineCnt = RegInit(0.U(12.W))

  io.streamOut.tvalid := io.streamIn.tvalid
  io.streamIn.tready := io.streamOut.tready

  io.streamOut.tdata := 0.U
  io.streamOut.tlast := false.B
  io.streamOut.tuser := false.B

  when (io.streamOut.tready && io.streamIn.tvalid) {

    pxlCnt := pxlCnt + 1.U

    // Last pixel in line
    when (pxlCnt === lineLen.U - 1.U) {
      lineCnt := lineCnt + 1.U
      pxlCnt := 0.U
    }

    when ((lineCnt & 1.U) === 1.U) {
      when ((pxlCnt & 1.U) === 1.U) {
        io.streamOut.tdata := Cat(
          io.streamIn.tdata(23, 16),
          io.streamIn.tdata(31, 24),
          io.streamIn.tdata(15, 8)
        )
      } .otherwise {
        io.streamOut.tdata := Cat(
          io.streamIn.tdata(7, 0),
          io.streamIn.tdata(15, 8),
          io.streamIn.tdata(31, 24)
        )
      }
    } .otherwise {
      when ((pxlCnt & 1.U) === 1.U) {
        io.streamOut.tdata := Cat(
          io.streamIn.tdata(31, 24),
          io.streamIn.tdata(15, 8),
          io.streamIn.tdata(7, 0)
        )
      } .otherwise {
        io.streamOut.tdata := Cat(
          io.streamIn.tdata(15, 8),
          io.streamIn.tdata(31, 24),
          io.streamIn.tdata(23, 16)
        )
      }
    }
  }
}
