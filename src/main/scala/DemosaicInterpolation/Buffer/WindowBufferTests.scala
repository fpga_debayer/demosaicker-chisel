package DemosaicInterpolation.Buffer

import chisel3.iotesters.{PeekPokeTester, Driver, ChiselFlatSpec}
import java.nio.file.{Files, Paths}
import java.io.BufferedOutputStream
import java.io.FileOutputStream

class WindowBufferTests(c: WindowBuffer) extends PeekPokeTester(c) {

  val x_res = 4
  val y_res = 4

  // small test frame
  var pixels = List( 0x1122, 0xaabb, 0x1122, 0xaabb,
                     0x2233, 0xbbcc, 0x2233, 0xbbcc,
                     0x3344, 0xccdd, 0x3344, 0xccdd)

  var inPixel = 0
  var outPixel = 0

  var done = false
  var steps = 0
  var valid = true

  poke(c.io.streamOut.tready, 1)
  poke(c.io.streamIn.tvalid, 0)
  step(100)

  while (!done) {

    if (steps == 9) {
      poke(c.io.streamOut.tready, 0)
    }
    if (steps == 10) {
      poke(c.io.streamOut.tready, 1)
    }


    print("pixel " + inPixel + " valid: " + valid + " ready: " + peek(c.io.streamIn.tready) + "\n")

    if (inPixel < (y_res -  1) * x_res && valid == true) {
      poke(c.io.streamIn.tdata, pixels(inPixel))
      poke(c.io.streamIn.tvalid, 1)
      if(peek(c.io.streamIn.tready) == 1) {
        inPixel = inPixel + 1
      }
    } else {
      poke(c.io.streamIn.tvalid, 0)
      poke(c.io.streamIn.tdata, 0)
    }

    if(peek(c.io.streamOut.tvalid) == 1 && peek(c.io.streamOut.tready) == 1) {
      print(
        "outpixel " + outPixel + " = " + peek(c.io.streamOut.tdata).byteValue + "\n" +
        ", step = " + steps + "\n")
      outPixel = outPixel + 1
    }

    //if (steps == 17000)
    if (outPixel == ((y_res - 1) * x_res))
      done = true;

    step(1)
    steps = steps + 1
  }



  poke(c.io.streamIn.tvalid, 0)
  poke(c.io.streamIn.tdata, 0)

  // add some cycles to let buffered line be fully transfered
  step(100)
}
