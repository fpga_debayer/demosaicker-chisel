package DemosaicInterpolation.Buffer

import chisel3._
import chisel3.util._

import DemosaicInterpolation.Bus.AXIStream

class WindowBuffer(val lineLen:Int, val lineCount:Int, val bufLen:Int, val bufCount:Int, val wordWidth:Int) extends Module {
  val io = IO(new Bundle{
    val streamIn = Flipped(new AXIStream(wordWidth * bufCount))
    val streamOut = new AXIStream(bufLen * bufCount * wordWidth)
  })

  val pxlCnt = RegInit(0.U(12.W))
  val lineCnt = RegInit(0.U(12.W))

  val ready_r = RegInit(false.B)
  val valid_r = RegInit(false.B)

  val data_r = RegInit(0.U((wordWidth * bufCount).W))
  val data_r_d = RegEnable(data_r, valid_r)

  io.streamOut.tvalid := valid_r
  io.streamIn.tready := RegNext(ready_r)
  io.streamOut.tlast := false.B
  io.streamOut.tuser := false.B
  io.streamOut.tdata := RegNext(io.streamOut.tdata)

  data_r := io.streamIn.tdata
  ready_r := io.streamOut.tready
  valid_r := io.streamIn.tvalid

  when (pxlCnt === 0.U) {
    io.streamOut.tvalid := false.B
  }


  when (ready_r && valid_r) {
    pxlCnt := pxlCnt + 1.U

    when (pxlCnt === lineLen.U) {
      pxlCnt := 1.U
      lineCnt := lineCnt + 1.U
      io.streamOut.tdata := RegNext(io.streamOut.tdata)
    } .otherwise {
      io.streamOut.tdata := Cat(
        data_r,
        data_r_d
      )
    }
  }
  when (ready_r) {
    when (pxlCnt === lineLen.U) {
      when (lineCnt === (lineCount - 2).U) {
        io.streamOut.tvalid := true.B
        lineCnt := lineCnt + 1.U
      }
    }
  }
}

/*
object windowBuffer extends App {
  if (args.length == 0) {
    chisel3.Driver.execute(args, () => new WindowBuffer(4, 4, 2, 2, 8))
  } else {
    iotesters.Driver.execute(args, () => new WindowBuffer(4, 4, 2, 2, 8)){ c => new WindowBufferTests(c) }
  }
}
*/
