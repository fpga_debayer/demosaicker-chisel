package DemosaicInterpolation.Buffer

import chisel3._
import chisel3.util._

import DemosaicInterpolation.Bus.AXIStream
import DemosaicInterpolation.Memory.Memory

class LineBuffer(val lineLen: Int, val lineCount: Int, val wordWidth: Int, val bufCount: Int) extends Module {
  val io = IO(new Bundle{
    val streamIn = Flipped(new AXIStream(wordWidth))
    val streamOut = new AXIStream(wordWidth * (bufCount - 1))
  })


  val sIdle :: sTransfer :: sDone :: Nil = Enum(3)

  val state = RegInit(sIdle)

  val lineBufs = VecInit(Seq.fill(bufCount){Module(new Memory(lineLen, wordWidth)).io})

  val done = RegInit(false.B)

  val initDone = RegInit(false.B)
  val memInitDone = RegInit(false.B)

  val enable = RegInit(false.B)

  val lastLines = RegInit(false.B)

  val ready = WireInit((io.streamOut.tready || !initDone) && enable)
  val valid = WireInit((io.streamIn.tvalid || lastLines) && enable)

  val ready_r = RegInit(false.B)
  val valid_r = RegInit(false.B)
  val data_r  = RegInit(0.U(wordWidth.W))

  val memWritePtr = RegInit(0.U(12.W))
  val memReadPtr = RegInit(0.U(12.W))

  val bufPtr  = RegInit(0.U(3.W))

  val lines   = RegInit(0.U(32.W))

  val lastData = RegInit(0.U((wordWidth * (bufCount - 1)).W))

  ready_r := ready
  valid_r := valid
  data_r  := io.streamIn.tdata

  io.streamIn.tready := ready_r
  io.streamOut.tvalid := valid_r && initDone

  lineBufs.foreach { buf =>
    buf.ena := 1.U
    buf.enb := 1.U
    buf.wea := 0.U
    buf.waddr := 0.U
    buf.raddr := 0.U
    buf.dataIn := 0.U
  }

  io.streamOut.tdata  := 0.U
  io.streamOut.tlast := false.B
  io.streamOut.tuser := false.B

  switch (state) {

    is (sIdle) {
      done := false.B
      enable := false.B
      initDone := false.B

      when (io.streamIn.tvalid) {
        state := sTransfer
        enable := true.B
      }
    }

    is (sTransfer) {

      io.streamOut.tdata := RegNext(io.streamOut.tdata)
      memInitDone := true.B

      when (ready_r && valid_r) {
        when(RegNext(memInitDone)) {
          memWritePtr := memWritePtr + 1.U
        }

        lineBufs(bufPtr).wea   := true.B
        lineBufs(bufPtr).waddr := memWritePtr
        lineBufs(bufPtr).dataIn := data_r

        when (initDone) {
          memReadPtr := memReadPtr + 1.U

          lineBufs.foreach { buf => buf.raddr := memReadPtr }

          when(!RegNext(ready_r) || !RegNext(valid_r)) {
            io.streamOut.tdata := lastData
          } .otherwise {
            switch (bufPtr) {
              is (2.U) {
                io.streamOut.tdata := Cat(
                  lineBufs(0).dataOut,
                  lineBufs(1).dataOut)
              }
              is (0.U) {
                io.streamOut.tdata := Cat(
                  lineBufs(1).dataOut,
                  lineBufs(2).dataOut)
              }
              is (1.U) {
                io.streamOut.tdata := Cat(
                  lineBufs(2).dataOut,
                  lineBufs(0).dataOut)
              }
            }
          }
          when (memReadPtr === lineLen.U - 1.U) {
            memReadPtr := 0.U
          }
        }

        when (memWritePtr === lineLen.U - 1.U && lines === 1.U) {
          memReadPtr := 1.U
        }

        // Line written
        when (memWritePtr === lineLen.U - 1.U) {

          memWritePtr := 0.U
          // increase line counter
          lines := lines + 1.U

          when (lines === lineCount.U - 2.U) {
            lastLines := true.B
          }

          // if 2 first lines filled init is done
          when (lines === 1.U) {
            initDone := true.B
          }

          when (bufPtr === bufCount.U - 1.U) {
            bufPtr := 0.U
          } .otherwise {
            bufPtr := bufPtr + 1.U
          }
        }

        when (memWritePtr === lineLen.U - 2.U) {
          when (lines === lineCount.U) {
            lastLines := false.B
          }
        }

      }

      when ( (!ready_r && RegNext(ready_r)) ||
             (!valid_r && RegNext(valid_r)) ) {
        switch (bufPtr) {
          is (2.U) {
            lastData := Cat(
              lineBufs(0).dataOut,
              lineBufs(1).dataOut)
          }
          is (0.U) {
            lastData := Cat(
              lineBufs(1).dataOut,
              lineBufs(2).dataOut)
          }
          is (1.U) {
            lastData := Cat(
              lineBufs(2).dataOut,
              lineBufs(0).dataOut)
          }
        }
      }
    }
  }
}

/*
object lineBuffer extends App {
  if (args.length == 0) {
    chisel3.Driver.execute(args, () => new LineBuffer(4, 4, 8, 3))
  } else {
    iotesters.Driver.execute(args, () => new LineBuffer(4, 4, 8, 3)){ c => new LineBufferTests(c) }
  }
}
*/
