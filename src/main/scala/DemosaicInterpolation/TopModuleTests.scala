package DemosaicInterpolation

import chisel3.iotesters.{PeekPokeTester, Driver, ChiselFlatSpec}
import java.nio.file.{Files, Paths}
import java.io.BufferedOutputStream
import java.io.FileOutputStream

class TopModuleTest(c: TopModule) extends PeekPokeTester(c) {

  // small test frame
  /*
  val x_res = 4
  val y_res = 4
  var pixels = List( 0x11, 0xaa, 0x11, 0xaa,
                     0x22, 0xbb, 0x22, 0xbb,
                     0x33, 0xcc, 0x33, 0xcc,
                     0x44, 0xdd, 0x44, 0xdd )
  */

  val x_res = 100
  val y_res = 100

  var pixels = Files.readAllBytes(Paths.get("frame.bin"))

  val outputData = new Array[Byte](x_res * y_res * 3)

  var inPixel = 0
  var outPixel = 0

  var done = false;
  var steps = 0
  var valid = true

  poke(c.io.streamOut.tready, 1)
  poke(c.io.streamIn.tvalid, 0)
  step(100)

  while (!done) {

    print("pixel " + inPixel + " valid: " + valid + " ready: " + peek(c.io.streamIn.tready) + "\n")

    if (inPixel < y_res * x_res && valid == true) {
      poke(c.io.streamIn.tdata, pixels(inPixel))
      poke(c.io.streamIn.tvalid, 1)
      if(peek(c.io.streamIn.tready) == 1) {
        inPixel = inPixel + 1
      }
    } else {
      poke(c.io.streamIn.tvalid, 0)
      poke(c.io.streamIn.tdata, 0)
    }

    if(peek(c.io.streamOut.tvalid) == 1 && peek(c.io.streamOut.tready) == 1) {
      outputData(outPixel *  3)     = ((peek(c.io.streamOut.tdata) >> 16) & 0xff).byteValue
      outputData(outPixel  * 3 + 1) = ((peek(c.io.streamOut.tdata) >>  8) & 0xff).byteValue
      outputData(outPixel  * 3 + 2) = ((peek(c.io.streamOut.tdata)      ) & 0xff).byteValue
      outPixel = outPixel + 1
    }

    //if (steps == 17000)
    if (outPixel == ((y_res - 1) * x_res))
      done = true;

    step(1)
    steps = steps + 1
  }

  val bos = new BufferedOutputStream(new FileOutputStream("frameOut.bin"))
  bos.write(outputData)
  bos.close


  poke(c.io.streamIn.tvalid, 0)
  poke(c.io.streamIn.tdata, 0)

  // add some cycles to let buffered line be fully transfered
  step(100)
}
